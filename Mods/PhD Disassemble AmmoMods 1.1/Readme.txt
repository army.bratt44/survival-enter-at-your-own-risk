PhD Disassemble Ammo and Mods - 1.1
This mod allows for the creation of a quest that breaks down ammo and mods into their component parts. Just select the item in your inventory, pull up recipes and then create the corresponding quest. Once created, just read the quest. The quest autocompletes and the component parts are added to your inventory. Simple. 

Most ammo can be converted in quantities of 10, 50, 250, or 1000 and does require a workbench. Now you can take all that ammo you don't use and break it down into raw materials so you can make ammo you do need. 

Ever been frustrated that your inventory is full and you're only halfway through a POI. Now you can breakdown all the mods you find along the way into raw material instead of letting them fill your inventory or having to leave them behind. 

The mod comes in two flavors Vanilla and Firearms 2. Vanilla is for use with the base game. Firearms 2 is for use with Mayic's Firearms 2 mod and includes all his ammo and mods as well. 

This is my first mod so I would very much enjoy your feedback.

:: REQUIREMENTS Template::
• Verions 7.4 
• Firearms 2 (if using that version of the mod)


:: FEATURES Template::
• Disassemble ammo into its component parts at a workbench
• Disassemble mods into their component parts

::  CREDITS ::
♦ Elucidus4 for coming up with the original idea for this mod
