﻿perkASMiningToolsDesc,progression,Perk,New,"+1% Block Damage and Resource Gain\n+0.3% Stamina Reduction\nper level",,,,,
perkASBluntDesc,progression,Perk,New,"+0.3% Damage and Stamina Reduction\nper level",,,,,
perkASBladedDesc,progression,Perk,New,"+0.3% Damage and Stamina Reduction\nper level",,,,,
perkASArcheryDesc,progression,Perk,New,"+0.3% Damage\n+0.5% Accuracy and Reload Speed\nper level",,,,,
perkASPistolsDesc,progression,Perk,New,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level",,,,,
perkASShotgunsDesc,progression,Perk,New,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level",,,,,
perkASRiflesDesc,progression,Perk,New,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level",,,,,
perkASAutomaticsDesc,progression,Perk,New,"+0.3% Damage\n+0.5% Accuracy, Reload Speed, and Recoil Reduction\nper level",,,,,
magazinePerkASMiningToolsDesc,items,Book,New,Grants you one level of the Action Skill Mining Tools.,,,,,
magazinePerkASBluntDesc,items,Book,New,Grants you one level of the Action Skill Blunt Weapons.,,,,,
magazinePerkASBladedDesc,items,Book,New,Grants you one level of the Action Skill Bladed Weapons.,,,,,
magazinePerkASArcheryDesc,items,Book,New,Grants you one level of the Action Skill Archery Weapons.,,,,,
magazinePerkASPistolsDesc,items,Book,New,Grants you one level of the Action Skill Pistol Weapons.,,,,,
magazinePerkASShotgunsDesc,items,Book,New,Grants you one level of the Action Skill Shotgun Weapons.,,,,,
magazinePerkASRiflesDesc,items,Book,New,Grants you one level of the Action Skill Rifle Weapons.,,,,,
magazinePerkASAutomaticsDesc,items,Book,New,Grants you one level of the Action Skill Automatic Weapons.,,,,,